/************************************************************************//**
 * \brief This module allows to manage (mainly read and write) from flash
 * memory chips such as S29GL032.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2015
 * \defgroup flash flash
 * \{
 ****************************************************************************/

#ifndef _FLASH_H_
#define _FLASH_H_

#include <stdint.h>
#include "util.h"

/// Write data buffer length in words
#define FLASH_CHIP_WBUFLEN	16

typedef void (*completion_cb)(int err, void *ctx);

// Maximum number of flash regions. CFI standard allows up to 4
#define FLASH_REGION_MAX 4

/**
 * \brief Metadata of a flash region, consisting of several sectors.
 * \{/
 */
struct flash_region {
	uint32_t start_addr;    //< Sector start address
	uint16_t num_sectors;   //< Number of sectors
	uint16_t sector_len;	//< Sector length in 256 byte units
};
/** \} */

/**
 * \brief Metadata of a flash chip, describing memory layout.
 * \{
 */
struct flash_chip {
	uint32_t len;                  //< Length of chip in bytes
	uint16_t num_regions;          //< Number of regions in chip
	struct flash_region region[FLASH_REGION_MAX]; //< Region data array
};

/*
 * Public functions
 */

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************//**
 * \brief Module initialization. Configures the 68k bus.
 ****************************************************************************/
int16_t flash_init(void);

/************************************************************************//**
 * \brief Writes the manufacturer ID query command to the flash chip.
 *
 * \return The manufacturer ID code.
 ****************************************************************************/
void flash_ids_get(uint8_t ids[4]);

/************************************************************************//**
 * \brief Programs a word to the specified address.
 *
 * \param[in] addr The address to which data will be programmed.
 * \param[in] data Data to program to the specified address.
 *
 * \warning Doesn't poll until programming is complete
 ****************************************************************************/
void flash_prog(uint32_t addr, uint16_t data);

/************************************************************************//**
 * \brief Programs a buffer to the specified address range.
 *
 * \param[in] addr The address of the first word to be written
 * \param[in] data The data array to program to the specified address range.
 * \param[in] wLen The number of words to program, contained on data.
 * \return The number of words successfully programed.
 *
 * \note wLen must be less or equal than 16.
 * \note If addr-wLen defined range crosses a write-buffer boundary, all the
 *       requested words will not be written. To avoid this situation, it
 *       is advisable to write to addresses having the lower 4 bits (A1~A5)
 *       equal to 0.
 ****************************************************************************/
uint_fast8_t flash_write_buf_block(uint32_t addr, const uint16_t *data, uint8_t wLen);
 

/************************************************************************//**
 * Erases the complete flash chip.
 *
 * \return '0' the if erase operation completed successfully, '1' otherwise.
 ****************************************************************************/
uint_fast8_t flash_chip_erase_block(void);

/************************************************************************//**
 * Erases a complete flash sector, specified by addr parameter.
 *
 * \param[in] addr Address contained in the sector that will be erased.
 * \return '0' if the erase operation completed successfully, '1' otherwise.
 ****************************************************************************/
uint_fast8_t flash_sect_erase_block(uint32_t addr);

/************************************************************************//**
 * Erases a flash memory range.
 *
 * \param[in] addr Address base for the range to erase.
 * \param[in] len  Length of the range to erase
 * \return '0' if the erase operation completed successfully, '1' otherwise.
 ****************************************************************************/
uint_fast8_t flash_range_erase_block(uint32_t addr, uint32_t len);

void flash_chip_erase(void *ctx);

void flash_sector_erase(uint32_t addr, void *ctx);

int flash_range_erase(uint32_t addr, uint32_t len);

int flash_write_buf(uint32_t addr, uint16_t *data, uint16_t wlen, void *ctx);

void flash_poll_proc(void);

int flash_write_long(uint32_t addr, uint16_t *data, uint16_t wlen);

void flash_completion_cb_set(completion_cb cb);

const struct flash_chip *flash_chip_get(void);

#ifdef __cplusplus
}
#endif

/** \} */

#endif /*_FLASH_H_*/

