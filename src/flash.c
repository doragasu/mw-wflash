#include <string.h>
#include "flash.h"
#include "util.h"

// Put function in .data (RAM) instead of the default .text
//#define RAM_SECT __attribute__((section(".ramprog")))
#define RAM_SECT
// Avoid function to be inlined by compiler
#define NO_INL __attribute__((noinline))

// Base address for the CFI data
#define CFI_BASE 0x10
// Offset in the CFI data buffer
#define CFI_LENGTH_OFF (0x27 - CFI_BASE)
#define CFI_NUM_REGIONS_OFF (0x2C - CFI_BASE)
#define CFI_REGION_DATA_OFF (0x2D - CFI_BASE)

enum PACKED poll_type {
	FLASH_POLL_NONE = 0,
	FLASH_POLL_DATA,
	FLASH_POLL_SECT,
	FLASH_POLL_RANGE,
	FLASH_POLL_CHIP,
	FLASH_POLL_MAX
};

struct write_long_data {
	uint16_t *buf;
	uint32_t addr;
	uint16_t buf_wlength;
	uint16_t wpos;
};

struct erase_range {
	uint32_t start;
};

static struct poll_data {
	completion_cb cb;
	void *ctx;
	uint32_t addr;
	union {
		struct erase_range erase;
		struct write_long_data write;
	};
	uint8_t data;
	enum poll_type type;
} poll;

static struct flash_chip flash = {};

RAM_SECT static void bus_write8(uint32_t addr, uint8_t data)
{
	volatile uint8_t *p_addr = (uint8_t*)addr;

	*p_addr = data;
}

RAM_SECT static uint8_t bus_read8(uint32_t addr)
{
	volatile uint8_t *p_addr = (uint8_t*)addr;

	return *p_addr;
}

RAM_SECT static void bus_write16(uint32_t addr, uint16_t data)
{
	volatile uint16_t *p_addr = (uint16_t*)addr;

	*p_addr = data;
}

RAM_SECT static void unlock(void)
{
	bus_write8(0xAAB, 0xAA);
	bus_write8(0x555, 0x55);
}

RAM_SECT static void reset(void)
{
	bus_write8(1, 0xF0);
}

static uint8_t data_poll(uint32_t addr, uint8_t data)
{
	uint8_t read;

	// Poll while DQ7 != data(7) and DQ5 == 0 and DQ1 == 0
	do {
		read = bus_read8(addr);
	} while (((data ^ read) & 0x80) && ((read & 0x22) == 0));

	// DQ7 must be tested after another read, according to datasheet
//	if (((data ^ read) & 0x80) == 0) return 0;
	read = bus_read8(addr);
	if (((data ^ read) & 0x80) == 0) return 0;
	// Data has not been programmed, return with error. If DQ5 is set, also
	// issue a reset command to return to array read mode
	if (read & 0x20) {
		reset();
	}
	// If DQ1 is set, issue the write-to-buffer-abort-reset command.
	if (read & 0x02) {
		unlock();
		reset();
	}
	return 1;
}

static uint_fast8_t erase_poll(uint32_t addr)
{
	uint8_t read;

	// Wait until DQ7 or DQ5 are set
	do {
		read = bus_read8(addr);
	} while (!(read & 0xA0));

	// If DQ5 is set, an error has occurred. Also a reset command needs to
	// be sent to return to array read mode.
	if (!(read & 0x80)) {
		return 0;
	}

	reset();
	return 1;
	//return (read & 0x80) != 0;
}

// buf must be 45 byte long or greater
RAM_SECT NO_INL static void cfi_read(uint8_t *buf)
{
	// CFI Query
	// NOTE: all chips I have seen accept the CFI Query command while in
	// autoselect mode, but entering autoselect is not required.
	bus_write8(0xAB, 0x98);

	// Read from 0x10 to 0x3C (45 bytes) in byte mode
	for (uint16_t i = 0, addr = 0x21 ; i < 45; i++, addr += 2) {
		buf[i] = bus_read8(addr);
	}

	// Exit CFI Query
	reset();
}

static int16_t metadata_populate(const uint8_t *cfi)
{
	flash.len = 1<<cfi[CFI_LENGTH_OFF];
	flash.num_regions = cfi[CFI_NUM_REGIONS_OFF];

	uint32_t start_addr = 0;
	const uint8_t *cfi_reg = cfi + CFI_REGION_DATA_OFF;
	for (uint16_t i = 0; i < flash.num_regions; i++) {
		struct flash_region *reg = &flash.region[i];

		reg->start_addr = start_addr;
		reg->num_sectors = *cfi_reg++ + 1;;
		reg->num_sectors |= 256 * *cfi_reg++;
		reg->sector_len = *cfi_reg++;
		reg->sector_len |= 256 * *cfi_reg++;

		start_addr += ((uint32_t)reg->sector_len * 256) * reg->num_sectors;
	}

	// The end of computed sector lengths should match the flash length
	if (start_addr != flash.len) {
		return -1;
	}

	return 0;
 }
 
int16_t flash_init(void)
{
	memset(&poll, 0, sizeof(struct poll_data));

	static const uint8_t cfi_check[] = {
		0x51, 0x52, 0x59, 0x02, 0x00
	};
	uint8_t cfi[45];

	// Read CFI data
	cfi_read(cfi);
 
	// Check response contains QRY and algorithm is AMD compatible
	for (uint16_t i = 0; i < sizeof(cfi_check); i++) {
		if (cfi[i] != cfi_check[i]) {
			// No flash chip installed, or chip not supported
			return -1;
		}
	}

	// Check number of regions is coherent and populate metadata
	const uint8_t num_regions = cfi[CFI_NUM_REGIONS_OFF];
	if (!num_regions || num_regions > FLASH_REGION_MAX) {
		// Between 1 and 4 regions are supported
		return -1;
	}

	return metadata_populate(cfi);
 }

void flash_ids_get(uint8_t ids[4])
{
	unlock();
	// Enter autoselect mode
	bus_write8(0xAAB, 0x90);
	ids[0] = bus_read8(0x01);
	ids[1] = bus_read8(0x03);
	ids[2] = bus_read8(0x1D);
	ids[3] = bus_read8(0x1F);
	// Exit autoselect mode
	reset();
}

void flash_prog(uint32_t addr, uint16_t data)
{
	unlock();
	bus_write8(0xAAB, 0xA0);
	bus_write16(addr, data);
}

uint_fast8_t flash_write_buf_block(uint32_t addr, const uint16_t *data, uint8_t wLen)
{
	// Number of words to write
	uint8_t wc;
	uint8_t i;
	uint32_t pos;

	// Check maximum write length
	if (wLen > flash.len) {
		return 0;
	}

	// Compute the number of words to write minus 1. Maximum number is 15,
	// but without crossing a write-buffer page
	wc = MIN(wLen, FLASH_CHIP_WBUFLEN - ((addr>>1) &
				(FLASH_CHIP_WBUFLEN - 1))) - 1;
	// Unlock and send Write to Buffer command
	unlock();
	bus_write16(addr, 0x25);
	// Write word count - 1
	bus_write16(addr, wc);

	// Write data to bufffer
	for (i = 0, pos = addr; i <= wc; i++, pos+=2) {
		bus_write16(pos, data[i]);
	}
	// Write program buffer command
	bus_write16(addr, 0x29);
	// Poll until programming is complete
	if (data_poll(addr - 1, data[i - 1] & 0xFF)) {
		return 0;
	}

	// Return number of elements (words) written
	return i;
}

static void erase_unlock(void)
{
	unlock();
	bus_write8(0xAAB, 0x80);
	bus_write8(0xAAB, 0xAA);
	bus_write8(0x555, 0x55);
}

uint_fast8_t flash_chip_erase_block(void)
{
	// Unlock and write chip erase sequence
	erase_unlock();
	bus_write8(0xAAB, 0x10);
	// Poll until erase complete
	return erase_poll(1);
}

uint_fast8_t flash_sect_erase_block(uint32_t addr)
{
	erase_unlock();
	bus_write8(addr + 1, 0x30);
	// Wait until erase starts (polling DQ3)
	while (!(bus_read8(addr + 1) & 0x08));
	// Poll until erase complete
	return erase_poll(addr);
}

int16_t flash_sector_limits(uint32_t addr, uint32_t *start, uint32_t *next)
{
	if (addr >= flash.len) {
		// Requested address does not fit in chip
		return -1;
	}

	// Find in which region is the address
	uint16_t reg_num = flash.num_regions - 1;
	while (addr < flash.region[reg_num].start_addr) {
		reg_num--;
	}

	// Compute limits
	const struct flash_region *reg = &flash.region[reg_num];
	const uint32_t sect_len = ((uint32_t)reg->sector_len) * 256;
	const uint32_t base = addr - reg->start_addr;
	const uint32_t trim = (base & ~(sect_len - 1)) + reg->start_addr;
	if (start) {
		*start = trim;
	}
	if (next) {
		*next = trim + sect_len;
	}

	return 0;
}

uint_fast8_t flash_range_erase_block(uint32_t addr, uint32_t len)
{
	uint32_t sect_start;
	uint32_t pos = addr + len - 1;

	do {
		flash_sector_limits(pos, &sect_start, NULL);
		flash_sect_erase_block(sect_start);
		pos = sect_start - 1;
	} while (sect_start > addr);
 
 	return 0;
 }

void flash_chip_erase(void *ctx)
{
	// Unlock and write chip erase sequence
	erase_unlock();
	bus_write8(0xAAB, 0x10);
	poll.type = FLASH_POLL_CHIP;
	poll.addr = 1;
	poll.data = 0xFF;
	poll.ctx = ctx;
}

void flash_sector_erase(uint32_t addr, void *ctx)
{
	// Obtain the sector address
	// Unlock and write sector address erase sequence
	addr++; // Need odd address to access low byte
	erase_unlock();
	bus_write8(addr, 0x30);
	poll.type = FLASH_POLL_SECT;
	poll.addr = addr;
	poll.data = 0xFF;
	poll.ctx = ctx;
}

static void range_erase_cb(int err, void *ctx)
{
	if (err) {
		poll.cb = ctx;
		if (poll.cb) {
			poll.cb(1, NULL);
		}
		return;
	}
	if (poll.addr > poll.erase.start) {
		// Relaunch sector erase with new sector
		flash_sector_limits(poll.addr - 2, &poll.addr, NULL);
		flash_sector_erase(poll.addr, ctx);
	} else {
		// Done
		poll.cb = ctx;
		if (poll.cb) {
			poll.cb(0, NULL);
		}
	}
}

int flash_range_erase(uint32_t addr, uint32_t len)
{
	if (!len) {
		return 0;
	}

	uint32_t limit = addr + len - 1;
	if (limit >= flash.len) {
 		return 1;
 	}

	void *ctx = poll.cb;

	poll.cb = range_erase_cb;
	// Store start address
	poll.erase.start = addr + 1; // Use odd address
	flash_sector_limits(limit, &poll.addr, NULL);
	flash_sector_erase(poll.addr, ctx);

	return 0;
}

int flash_write_buf(uint32_t addr, uint16_t *data, uint16_t wlen, void *ctx)
{
	// Start address
	uint32_t start;
	// Number of words to write
	uint8_t wc;
	// Index
	uint8_t i;

	// Obtain the sector address
	start = addr;
	// Compute the number of words to write minus 1. Maximum number is 15,
	// but without crossing a write-buffer page
	wc = MIN(wlen, FLASH_CHIP_WBUFLEN - ((addr>>1) &
				(FLASH_CHIP_WBUFLEN - 1))) - 1;
	// Unlock and send Write to Buffer command
	unlock();
	bus_write16(start, 0x25);
	// Write word count - 1
	bus_write16(start, wc);

	// Write data to bufffer
	for (i = 0; i <= wc; i++, addr+=2) {
		bus_write16(addr, data[i]);
	}
	// Write program buffer command
	bus_write16(start, 0x29);
	poll.type = FLASH_POLL_DATA;
	poll.addr = addr - 1;
	poll.data = data[i - 1];
	poll.ctx = ctx;

	// Return number of elements (words) to write
	return i;
}

static void flash_write_long_cb(int err, void *ctx)
{
	int written;
	struct write_long_data *w = &poll.write;

	if (err) {
		goto out;
	}
	if (poll.write.wpos >= poll.write.buf_wlength) {
		goto out;
	}
	written = flash_write_buf(w->addr, w->buf, w->buf_wlength - w->wpos,
			ctx);
	if (written <= 0) {
		err = 1;
		goto out;
	}

	poll.write.wpos += written;
	poll.write.addr += 2 * written;
	poll.write.buf += written;

	return;


out:
	poll.type = FLASH_POLL_NONE;
	poll.cb = ctx;
	if (poll.cb) {
		poll.cb(err, ctx);
	}
}

int flash_write_long(uint32_t addr, uint16_t *data, uint16_t wlen)
{
	void *ctx = poll.cb;
	int written;

	poll.cb = flash_write_long_cb;
	poll.write.addr = addr;
	poll.write.buf = data;
	poll.write.buf_wlength = wlen;
	poll.write.wpos = 0;
	written = flash_write_buf(addr, data, wlen, ctx);
	if (written <= 0) {
		return written;
	}
	poll.write.wpos += written;
	poll.write.addr += 2 * written;
	poll.write.buf += written;

	return 0;
}

void flash_poll_proc(void)
{
	uint8_t read;
	int err = 0;

	if (!poll.type) {
		return;
	}

	read = bus_read8(poll.addr);
	if ((read & 0x80) == (poll.data & 0x80)) {
		goto complete;
	}
	if (read & 0x20) {
		read = bus_read8(poll.addr);
		if ((read & 0x80) == (poll.data & 0x80)) {
			goto complete;
		} else {
			err = 1;
			reset();
			goto complete;
		}
	}

	return;

complete:
	poll.type = FLASH_POLL_NONE;
	if (poll.cb) {
		poll.cb(err, poll.ctx);
	}
}

void flash_completion_cb_set(completion_cb cb)
{
	poll.cb = cb;
}

const struct flash_chip *flash_chip_get(void)
{
	return &flash;
}
